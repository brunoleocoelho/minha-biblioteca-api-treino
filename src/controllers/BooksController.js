const db = require('../database/db-connection')

const notRentedBookIds = `select ir.bookId from item_reservation as ir 
                        left join reservation as res on res.id = ir.reservationId
                        where res.dateEnd > curdate()`;

const Book = {
    /** Retorna uma lista com os livros disponíveis do dia, e com paginação */
    getBookList: (req, res) => {
        const page = (req.query.page) || 1;
        const pageSize = (req.query.pageSize) || 10;
        const qryLimit = ` limit ${(page*pageSize)-pageSize},${pageSize} `;
        const qry = `select book.*, 
            (select name from author where book.authorId = author.id) as author,
            (select description from category where book.categoryId = category.id) as category
            from book 
            where book.id not in ( ${notRentedBookIds} )
            order by title, id ${qryLimit}`;
        
        const conn = db.conn()
        conn.query(qry, (err, result, fields) => {
            if (err) res.json(err);
            const jsonResp = {
                hasNext: (result.length===pageSize),
                books: result
            }
            res.json(jsonResp);
        })
    },

    /** Busca um livro pelo ttulo */
    getSearchBook: (req, res) => {
        const term = req.query.q
        const page = (req.query.page) || 1;
        const pageSize = (req.query.pageSize) || 10;
        const qryLimit = ` limit ${(page*pageSize)-pageSize},${pageSize} `;
        const qry = `select book.*, 
            (select name from author where book.authorId = author.id) as author,
            (select description from category where book.categoryId = category.id) as category
            from book 
            where book.title like '%${term}%'
                and book.id not in ( ${notRentedBookIds} )
            order by title, id ${qryLimit}`;
        
        const conn = db.conn()
        conn.query(qry, (err, result, fields) => {
            if (err) res.json(err);
            const jsonResp = {
                hasNext: (result.length===pageSize),
                books: result
            }
            res.json(jsonResp);
        })
    },

    /** Adiiciona uma reserva de livros */
    newReservation: (req, res) => {
        const { reservation } = req.body

        let qry1 = 'insert into reservation (dateStart, dateEnd, customerId) values (?); ';
        let qry2 = '(select max(id) as reservId from reservation)';
        let qry3 = 'insert into item_reservation (reservationId, bookId) values ?;';

        let valQry1 = [reservation.dateStart, reservation.dateEnd, reservation.user.id]
        let valQry3 = '';

        let jsonResp = {};
        const pool = db.pool()
        pool.getConnection((err, connection) => {
            // query 1: cria a reserva
            connection.query(qry1, [valQry1], (err, rows) => {
                if(err) connection.release();
                jsonResp = {...jsonResp, rows}

                // query 2: recupera o id da reserva
                connection.query(qry2, (err, rows) => { 
                    if (err) connection.release();
                    newReservId = rows[0].reservId.toString().padStart(6,'0')
                    valQry3 = reservation.items.map(item => ([newReservId, item.id]))
                    jsonResp = {...jsonResp, rows}
                
                    // query 3: insere os itens
                    connection.query(qry3, [valQry3], (err, rows) => {
                        if (err) connection.release();
                        jsonResp = {...reservation, id: newReservId, status: rows}
                        console.log(jsonResp)
                        res.json(jsonResp) // <-- RESPOSTA FINAL JSON
                    })
                });

             });
        })        
    }
}

module.exports = Book