const db = require('../database/db-connection')
const atob = require('atob');

const Customer = {
    /** POST Faz a 'autenticação' do cliente usuário */
    authCustomer: (req, res) => {
        const encoded = req.body;
        if (!encoded) res.json({})
        
        const user = JSON.parse(atob(encoded.userData))
        if (!user) req.json({})
        
        const qry = `select id, name, email 
            from customer 
            where email = '${user.email}'
            and password = '${user.password}'`;

        const conn = db.conn();
        conn.query(qry, (err, result, fields) => {
            if (err) res.json(err);
            if (result.length===0) res.json({});
            let user = result[0];
            res.json(user)
        })
    },

    /** Adiciona um novo customer/usuario */
    newCustomer: (req, res) => {
        const encoded = req.body;
        if (!encoded) res.json({})

        const user = JSON.parse(atob(encoded.userData)) // { name, email, password }
        if (!user) req.json({})

        const qry1 = `select count(*) as qtd from customer where email = ?`;
        const qry2 = `insert into customer (name, email, password) values ?`;
        console.log('qry1', qry1)
        console.log('qry2', qry2)

        const valQry1 = [user.email]
        const valQry2 = [user.name, user.email, user.password]
        console.log('valQry1', valQry1)
        console.log('valQry2', valQry2)

        const pool = db.pool();
        pool.getConnection((err, conn) => {
            conn.query(qry1, [valQry1], (err, result) => {
                if (err){
                    console.log(err)
                    connection.release();
                    res.json(err);
                }

                let existUsr = result[0].qtd
                if (!existUsr) {
                    conn.query(qry2, [valQry2], (err, result) => {
                        if (err){
                            console.log(err)
                            connection.release();
                            res.json(err);
                        }
        
                        let user = {...user, status: result[0]};
                        delete user['password'];
                        res.json(user);
                    })
                }
                else {
                    res.json({error: `usuario com email ${user.email} já existe`});
                }
            })
            
        })
    }
}

module.exports = Customer