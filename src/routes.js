const express = require('express');
const router = express.Router();

const BooksController = require('../src/controllers/BooksController');
const CustomersController = require('./controllers/CustomersController');

// Books
router.get('/book/list/:page?', BooksController.getBookList);
router.get('/book/search/', BooksController.getSearchBook);
router.post('/reservation/', BooksController.newReservation);

// Customers
router.post('/auth', CustomersController.authCustomer);
router.post('/customer/new', CustomersController.newCustomer);

// Exporta as routas criadas
module.exports = router;