const mysql = require('mysql');

const credentials = {
  host     : 'localhost',
  user     : 'root',
  password : 'abcd1234',
  database : 'bibliotecadb',
  multipleStatements: true
}

const conn = () => mysql.createConnection({...credentials, multipleStatements: true});

const pool = () => mysql.createPool({...credentials, connectionLimit : 10});

module.exports = {
  conn,
  pool
};