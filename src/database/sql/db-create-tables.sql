-- MYSQL DATABASE
-- Arquivo contendo criação das tabelas
-- Host: localhost    Database: bibliotecadb
-- Bruno Leo Coelho   Date: 2019-07-14
-- Import statement: mysql -u root -p -h localhost bibliotecadb < db-create-tables.sql
-- ------------------------------------------------------

DROP DATABASE IF EXISTS bibliotecadb;
CREATE DATABASE bibliotecadb;
USE bibliotecadb;

/* TABELA author */
DROP TABLE IF EXISTS author;
CREATE TABLE author (
  id int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  name varchar(150) NOT NULL,

  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;


/*  TABELA category */
DROP TABLE IF EXISTS category;
CREATE TABLE category (
  id int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  description varchar(30) NOT NULL,

  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;


/*  TABELA book */
DROP TABLE IF EXISTS book;
CREATE TABLE book (
  id int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  title varchar(150) NOT NULL,
  authorId int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  publisher varchar(45) DEFAULT NULL,
  categoryId int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  isbn char(13) NOT NULL,
  image_url varchar(300) DEFAULT NULL,
  
  PRIMARY KEY (id),
  FOREIGN KEY (authorId) REFERENCES author (id),
  FOREIGN KEY (categoryId) REFERENCES category (id)
) DEFAULT CHARSET=utf8;


/*  TABELA customer */
DROP TABLE IF EXISTS customer;
CREATE TABLE customer (
  id int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  name varchar(150) NOT NULL,
  email varchar(150) DEFAULT NULL,
  password varchar(30) DEFAULT NULL,

  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;


/*  TABELA reservation */
DROP TABLE IF EXISTS reservation;
CREATE TABLE reservation (
  id int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  dateStart date NOT NULL,
  dateEnd date NOT NULL,
  customerId int(6) UNSIGNED ZEROFILL NOT NULL,
  protocol varchar(40) DEFAULT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (customerId) REFERENCES customer (id)
) DEFAULT CHARSET=utf8;


/*  TABELA item_reservation */
DROP TABLE IF EXISTS item_reservation;
CREATE TABLE item_reservation (
  reservationId int(6) UNSIGNED ZEROFILL NOT NULL,
  bookId int(6) UNSIGNED ZEROFILL NOT NULL,

  PRIMARY KEY (reservationId,bookId),
  FOREIGN KEY (reservationId) REFERENCES reservation (id),
  FOREIGN KEY (bookId) REFERENCES book (id)
) DEFAULT CHARSET=utf8;
