-- MYSQL DATABASE
-- Arquivo contendo inserção de dados para as tabelas
-- Host: localhost    Database: bibliotecadb
-- Bruno Leo Coelho   Date: 2019-07-14
-- ------------------------------------------------------

USE bibliotecadb;

/* TABELA 'author' */
INSERT INTO `author` VALUES 
(0001,'Jorge Amado'),
(0002,'Macahado de Assis'),
(0003,'Paulo Coelho'),
(0004,'Clarice Linspector'),
(0005,'Abraham Silberrschatz'),
(0006,'Andrew Tanenbaum'),
(0007,'Julio Verne'),
(0008,'Stephen Hawking'),
(0009,'Greg Gagne'),
(0010,'Joseph Jacobs'),
(0011,'Guimarães e Lages'),
(0012,'Sun Tzu');

/*  TABELA 'category' */
INSERT INTO `category` VALUES 
(001,'Romance'),
(002,'Ficcao'),
(003,'Drama'),
(004,'Suspense'),
(005,'Infanto Juvenil'),
(006,'Tecnologia');

/*  TABELA 'book' */
INSERT INTO `book` VALUES 
(000001,'Sistemas Operacionais Modernos',0006,'Brasport',006,'9788574527611','https://images-na.ssl-images-amazon.com/images/I/611PHdp2x%2BL.jpg'),
(000002,'O Alquimista',0003,'Planeta',002,'9788584390670','https://d1pkzhm5uq4mnt.cloudfront.net/imagens/capas/97aacfa0ca94a9fac48257ad3dabe634881677c2.jpg'),
(000003,'O universo numa casca de noz',0008,'Mandarim',006,'9788500024054','http://statics.livrariacultura.net.br/products/capas_lg/194/42142194.jpg'),
(000004,'O misterio do coelho pensante e outros contos',0004,'Rocco',005,'9788581226071','https://d1pkzhm5uq4mnt.cloudfront.net/imagens/capas/94b1513408ddd36f6145fb7b5ba52bed0fce42c7.jpg'),
(000005,'Manuscrito Encontrado em Accra',0003,'Sextante',002,'9788575428221','https://images-na.ssl-images-amazon.com/images/I/81BJTVNnmGL.jpg'),
(000006,'Tres porquinhos',0010,'Ciranda Cultural',005,'9788538028604','https://www.cirandacultural.com.br/resizer/view/300/800/false/true/os-tres-porquinhos-1635.jpg'),
(000007,'A Hora da Estrela',0004,'Rocco',001,'9788532508126','http://statics.livrariacultura.net.br/products/capas_lg/729/170729.jpg'),
(000008,'Água Viva',0004,'Rocco',001,'9788532531407','https://img.travessa.com.br/livro/BA/26/266789be-2068-4937-a611-5c531f282f48.jpg'),
(000009,'Gabriela, Cravo e Canela',0001,'Companhia da Letras',001,'9788535920987','https://images-na.ssl-images-amazon.com/images/I/41bwRryjXHL.jpg'),
(000010,'Dom Casmurro',0002,'Globo Editora',001,'9788572322645','https://d1pkzhm5uq4mnt.cloudfront.net/imagens/capas/_edc14fb2f0db9111cec880cbcf2e48fdf0731f55.jpg'),
(000011,'Fundamentos de Sistemas Operacionais',0005,'LTC',006,'9788521622055','https://image.isu.pub/150820112732-b1ac288ec7038623a1527b18d126f95e/jpg/page_1_thumb_large.jpg'),
(000012,'A volta ao Mundo em 80 dias',0007,'Estrela Cultural',001,'9788422642138','http://statics.livrariacultura.net.br/products/capas_lg/354/2000177354.jpg'),
(000013,'Algoritmos e estruturas de dados',0011,'LTC',006,'9788521603788','https://img.travessa.com.br/livro/BA/43/43bddfa1-7dd0-4964-b9aa-57bb3cf5b2db.jpg'),
(000014,'A arte da guerra - Os treze capitulos',0012,'Jardim dos Livros',006,'9781590302255','https://images.livrariasaraiva.com.br/imagemnet/imagem.aspx/?pro_id=1391401&qld=90&l=430&a=-1');

/*  TABELA 'customer' */
INSERT INTO `customer` VALUES 
(000001,'Ana Souza','anass@uul.com.br','qwert7890'),
(000002,'Camila Silva','csilva@doc.com.br','poiu4321'),
(000003,'João Matos','joao@email.com','asdf1234'),
(000004,'Mateus Nunes','m.nune@mhost.com','zxcv0987');

/*  TABELA 'reservation' */
INSERT INTO `reservation` VALUES 
(000001,'2019-07-11','2019-07-21',000004,NULL),
(000002,'2019-07-12','2019-07-18',000002,NULL);

/*  TABELA 'item_reservation' */
INSERT INTO `item_reservation` VALUES 
(000001,000002),
(000001,000003),
(000002,000005);
