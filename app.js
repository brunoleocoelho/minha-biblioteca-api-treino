const express = require('express')
// const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()

// Parse URL-encoded bodies (as sent by HTML forms)
// app.use(express.urlencoded());
app.use(express.json()) 
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json())
app.use(cors())

app.use('/api', require('./src/routes'))

const port = 3001
app.listen(port, () => {
    console.log(`Biblioteca API rodando na porta ${port}.`)
})
